var globalPlaylist = [];
Vue.component('song',{
    props: ['position'],
    template: '<li><slot></slot><button @click="deleteSong(position)">Delete</button></li>',
    methods:{
        deleteSong(positionToDeleteFrom){
            globalPlaylist.splice(positionToDeleteFrom,1);
        }
    }
})
Vue.component('playlist',{
    template: '<ul><song v-for="(song, index) in songs" :position="index"><h3>{{ song }}</h3></song></ul>',
    data(){
        return{
            songs: globalPlaylist
        }
    }
})
new Vue({
    el: '#title',
    data: {
        title1: 'Vue Playlist'
    }
})
new Vue ({
    el: '.playlist',
    data: {
        newTitle: '',
        newArtist: '',
        playlist: globalPlaylist,
    },
    methods: {
        addSong(){

            globalPlaylist.push(this.newTitle + " " + this.newArtist);

        },
        removeSong(){
            globalPlaylist.pop();
        }
    }
})
