//define parent and child nested components for component playlist
Vue.component('song',{
    template: '<li class="list-group-item"><slot></slot></li>',
})
Vue.component('song-list',{
    template: '<ul class="list-group"><song v-for="list in lists" :key="list.songName"><h3>{{ list.songName }} - {{ list.artistName }}</h3><p>#{{ list.topCharts }} on the charts.</p></song></ul>',
    data(){
        return {
            lists: [
                {
                    songName: "Sunflower",
                    artistName: "Post Malone & Swae Lee','Panic! At The Disco",
                    topCharts: "4"
                }
            ]
        }
    },
})
new Vue({
    el: '#title',
    data: {
        name: 'Vue Playlist'
    }
})
new Vue ({
    el: '#play-list',
    data: {
        songName: ' ',
        artistName: ' ',
        lists: [

             //songName:('Sunflower','Highhopes','Going bad','A lot'),
             //artist:('Post Malone & Swae Lee','Panic! At The Disco','Meek Mill Featuring Drake','21 Savage'),
             //topCharts:(4,12,16,18)

        ]
    },
    methods: {
        addSong(){
        //  alert();
            var song = [this.songName, this.artistName];

            this.lists.push(song);

        },

    }
})
