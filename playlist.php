<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>VUE Playlist</title>
    </head>
    <body>
        <main>
            <section>
                <h1 id="title">{{ title1 }}</h1>
                <br /><hr />
                <div class="playlist">
                    <input v-model="newTitle" type="text" />
                    <input v-model="newArtist" type="text" />
                    <button @click='addSong'>Add</button>
                    <br /><hr />
                    <ul v-for="song in playlist">
                        <li>{{ song }}</li>

                    </ul>
                </div>

            </section>
            <br /><hr />
            <section>
                <div class="picture">
                    <a v-bind:href="newHref"><img :src="newSrc" height="100"/></a>
                    <br />
                    <input v-model="newHref" type="text" />
                    <input v-model="newSrc" type="text" />
                    <button @click='changeImg'>Change</button>
                </div>
            </section>
        </main>



        <script type="text/javascript" src="javascript/vue.js"></script>
        <script type="text/javascript" src="javascript/playlist.js"></script>
    </body>
</html>
