<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>VUE Playlist</title>
    </head>
    <body>
        <main>
            <section>
                <h1 id="title">{{ title1 }}</h1>
                <br /><hr />
                <div class="playlist">
                    
                    <label>Song Name:</label>
                    <input v-model="newTitle" @keyup.enter="addSong" type="text" />

                    <label>Artist:</label>
                    <input v-model="newArtist" @keyup.enter="addSong" type="text" />

                    <button @click='addSong'>Add</button>
                    <button @click='removeSong'>Remove</button>

                    <br /><hr />

                    <playlist></playlist>

                </div>

            </section>
            <br /><hr />

        </main>



        <script type="text/javascript" src="javascript/vue.js"></script>
        <script type="text/javascript" src="javascript/playlist-part2.js"></script>
    </body>
</html>
